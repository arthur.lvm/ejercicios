# Ejercicios



## Ejercicio de diagnostico


** Pre condiciones **
 Los numeros simultaños pueden ser elegidos indistintamente e imprimira el msj asociado,seguido de el proximo simultaneo con su primera letra en mayuscula

Se puede tener una sola lista de simultaño a comparar 
## Enunciado

Desarrollar un programa que muestre los números del 1 al 100 (inclusive), con la salvedad de:
Si el número es múltiplo de 3 debe reemplazar la salida por consola por **"fizz"**.
Si el número es múltiplo de 5 debe reemplazar la salida por consola por **"buzz"**.
Si el número es simultáneamente múltiplo de 3 y 5, debe reemplazar la salida por consola por **"fizzBuzz"**.
Si el número no cumple ninguno de esos requisitos, simplemente la salida por consola será el mismo número.
Ejemplo: 
para 1 la salida es 1.
para 3 la salida es **"fizz"**.
para 25 la salida es **"buzz"**.
para 60 la salida es **"fizzBuzz"**.



## Opcional

Una vez desarrollado el  programa anterior, pensar cómo resolver los siguientes requerimientos y si la manera en que construyó la solución lo permite. En caso de que no, mencionar las modificaciones necesarias para permitir los nuevos requerimientos.
Conocer el resultado fizzBuzz de un número en particular. Es decir, dado un número devolver según corresponda "fizz", "buzz", "fizzBuzz" o el número.
Por ejemplo fizzBuzz(3) -> “fizz”
Dada una lista de números, devolver los múltiplos de un número en particular.
Por ejemplo multiplosDe(3,[1,2,3,4,5,6,7]) -> [3,6]


## Para el funcionamiento

    **Diagnostico** 
        Es lo principal lo cual el resto de modulos se apoya en él
        Todos se ejecutan desde la linea de comando
    **Primer Enunciado**
       Enviando 2 numero, uno de inicio otro de fin.
            Los multiplos van por defecto 
    **SegundoEncunciado** 
        Se envia el numero el cual se quiere obtener el msj asociado
            Los multiplos y el msj van por defecto
    **SegundoEnunciado B **
        Se envia el multiplo por el cual se quiere comprar, seguido de los numeros a comparar 
