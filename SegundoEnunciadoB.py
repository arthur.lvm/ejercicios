from Diagnostico import Msj_multiplo
import sys

def devolver_multiplos_de(multiplo,args):
    lista_a_devolver = []
    obj = Msj_multiplo(int(multiplo))
    for numero in args:
        if obj.sos_multiplo(int(numero)):
            lista_a_devolver.append(numero)
    print(lista_a_devolver)
    return lista_a_devolver

if __name__ == '__main__':


    if len(sys.argv) >= 3:
        devolver_multiplos_de(sys.argv[1],sys.argv[2:])
    else:
        print("Ingrese un numero para obtener el mensaje correpondiente")